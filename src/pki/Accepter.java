package pki;

import org.apache.log4j.Logger;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Accepter implements Runnable {

    private static Logger logger = Logger.getLogger(Accepter.class);

    private Socket localSocket;
    private String remoteIp;
    private int remotePort;
    private int timeOut;
    private int soTimeout;

    private String protocal;
    private int tag;

    public Accepter(String protocal, int tag, Socket localSocket, String remoteIp, int remotePort, int timeOut, int soTimeout) {
        this.protocal = protocal;
        this.tag = tag;
        this.localSocket = localSocket;
        this.remoteIp = remoteIp;
        this.remotePort = remotePort;
        this.timeOut = timeOut;
        this.soTimeout = soTimeout;
    }

    @Override
    public void run() {
        try {
            //Socket remoteSocket = new Socket(remoteIp, remotePort);
            Socket remoteSocket = new Socket();
            remoteSocket.connect(new InetSocketAddress(remoteIp, remotePort), timeOut);
            remoteSocket.setSoTimeout(soTimeout);

            new Relay(this.protocal + "-Request", tag, localSocket.getInputStream(), remoteSocket.getOutputStream()).start();
            new Relay(this.protocal + "-Response", -1, remoteSocket.getInputStream(), localSocket.getOutputStream()).start();
        } catch (Throwable ex) {
            logger.error(ex);
        }
    }
}
